/**
 * defines some symbols, which will be used as methods of Resource class, etc.
 */

export const get: unique symbol = Symbol('get');
export const url: unique symbol = Symbol('url');
export const title: unique symbol = Symbol('title');
export const label: unique symbol = Symbol('label');
export const dsc: unique symbol = Symbol('dsc');
export const imgRepr: unique symbol = Symbol('imgRepr');
export const by: unique symbol = Symbol('by');
export const meta: unique symbol = Symbol('meta');

export const from: unique symbol = Symbol('from');
