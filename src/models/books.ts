import * as S from '../symbols';
import { Resource, Text } from './base';
import { purgeUndefined } from '../utils';
import { StillImageRepresentation } from './repr';
import { Selector, FragmentSelector, SvgFragmentSelectorChoice, IBBox } from './selectors';
import { ModelsRegistry } from '.';

export class ScannedBook extends Resource {
    public static [S.from]({ title, authors, coverId }: {title: string, authors?: string[], coverId?: string}): ScannedBook {
        const doc = {
            jsonClass: 'ScannedBook',
            title,
            author: authors,
            cover: coverId ? StillImageRepresentation.from({ fileId: coverId }) : undefined,
        };
        purgeUndefined(doc);
        return Resource.make<ScannedBook>(doc);
    }

    public jsonClass = 'ScannedBook';

    public title!: string;

    public author?: string[];

    public cover?: StillImageRepresentation;

    public [S.title](): Text {
        return Text.from({ chars: this.title });
    }

    public [S.by](): string[] {
        return this.author || [];
    }

    public [S.imgRepr]() {
        return this.cover ? { repr: this.cover } : null;
    }
}


export class Library extends Resource {
    public jsonClass = 'Library';

    public name!: string;

    public description?: string;

    public logo?: StillImageRepresentation;

    public [S.title](): Text {
        return Text.from({ chars: this.name });
    }

    public [S.imgRepr]() {
        return this.logo ? { repr: this.logo } : null;
    }
}


export class ScannedPage extends Resource {
    jsonClass = 'ScannedPage';

    public selector?: Selector;

    public [S.title]() {
        if (this.selector && Object.prototype.hasOwnProperty.call(this.selector, 'index')) {
            return Text.from({ chars: this.selector.index });
        }
        return null;
    }

    public [S.dsc]() {
        const descr = super[S.dsc]();
        if (descr) {
            return descr;
        }
        const title = this[S.title]();
        if (!title) {
            return null;
        }
        title.chars = `page ${title.chars} of book ${this.source || ''}`;
        return title;
    }
}

export class ImageRegion extends Resource {
    jsonClass = 'ImageRegion';

    source!: string;

    selector!: FragmentSelector | SvgFragmentSelectorChoice;

    public getBBox(): IBBox | null {
        return this.selector.imageBBox;
    }

    public [S.title]() {
        const bbox = this.getBBox();
        if (!bbox) {
            return null;
        }
        const { x, y, w, h } = bbox;
        const chars = `ImageRegion:- x:${x}, y:${y}, w:${w}, h:${h}`;
        return Text.from({ chars });
    }

    public [S.imgRepr]({ parent }: {parent?: Resource}) {
        if (!parent) {
            return null;
        }
        const parentImageRepr = parent[S.imgRepr]();
        if (!parentImageRepr) {
            return null;
        }
        return {
            repr: parentImageRepr.repr,
            selector: this.selector,
        };
    }
}

export class TextDocument extends Resource {
    jsonClass = 'TextDocument';

    title?: string;

    author?: string[];

    content?: string;

    components?: Text[];

    public [S.title]() {
        return this.title ? Text.from({ chars: this.title }) : null;
    }

    public [S.by]() {
        return this.author || [];
    }

    // eslint-disable-next-line class-methods-use-this
    public [S.imgRepr]({ parent }: {parent?: Resource}) {
        return parent ? parent[S.imgRepr]() : null;
    }
}


export class TextSection extends Resource {
    jsonClass = 'TexttSection';

    name?: string;

    content?: string;

    components?: Text[];

    relation_to_parent?: 'content' | 'attr';

    public [S.title]() {
        return this.name ? Text.from({ chars: this.name }) : null;
    }
}


ModelsRegistry.update({
    Library,
    ScannedBook,
    ScannedPage,
    ImageRegion,
    TextDocument,
    TextSection,
});
