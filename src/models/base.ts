import { purgeUndefined } from '../utils';
import * as S from '../symbols';

import { IResourceDoc, JsonObject, ModelsRegistry } from '.';
import { FragmentSelector, SvgFragmentSelectorChoice } from './selectors';
import { StillImageRepresentation, DataRepresentations } from './repr';


export class Resource extends JsonObject implements IResourceDoc {
    public jsonClass: string = 'Resource';

    public metadata?: Array<{ jsonClass: 'MetadataItem', label: string, value: any }>;

    public source?: string | string[] | null;

    public target?: string | string[] | null;

    public body?: any;

    public selector?: any;

    public representations?: DataRepresentations;

    public jsonClassLabel?: string;

    // tslint:disable-next-line: variable-name
    public _id?: string;

    public created?: string;

    public creator?: string;

    public resolvedPermissions?: ResolvedPermissions;

    // tslint:disable-next-line: variable-name
    public _reached_ids?: { [field: string]: string[] };

    // tslint:disable-next-line: variable-name
    public _in_links?: { [field: string]: string[] };

    [x: string]: any;

    /**
     * returns nested field if exists otherwise returns undefined
     * @param deepField - nested field string. ex: 'title.0.chars'
     */
    public [S.get]<T>(deepField: string): T | undefined {
        const deepGet = (doc: any, fieldParts: string[]): any => {
            if (fieldParts.length === 0) {
                return doc;
            }
            if (doc === undefined || doc === null || typeof (doc) !== 'object') {
                return undefined;
            }
            const [field, ...nestedFields] = fieldParts;
            if (!Object.prototype.hasOwnProperty.call(doc, field)) {
                return undefined;
            }
            return deepGet(doc[field], nestedFields);
        };
        return deepGet(this, deepField.split('.')) as T;
    }

    /**
     * title of resource
     */
    // eslint-disable-next-line
    public [S.title](params?: {[p: string]: any}): Text | null {
        return null;
    }

    /**
     * label of resource
     */
    // eslint-disable-next-line
    public [S.label](params?: { [p: string]: any }): Text | null {
        return null;
    }

    /**
     * description of resource
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public [S.dsc](params?: { [p: string]: any }): Text | null {
        const meta = this[S.meta]();
        const { description } = meta;
        if (description === undefined || description === null) {
            return null;
        }
        if (typeof (description) === 'object') {
            if (description.jsonClass === 'Text') {
                return description as Text;
            }
            return null;
        }
        return Resource.make<Text>({
            jsonClass: 'Text',
            chars: description,
        });
    }

    /**
     * 'by agent' of resource
     */
    // eslint-disable-next-line
    public [S.by](params?: { [p: string]: any }): string[] | null {
        return null;
    }

    /**
     * image representation of resource
     */
    // eslint-disable-next-line
    public [S.imgRepr](params?: { [p: string]: any }): {repr: StillImageRepresentation; selector?: FragmentSelector | SvgFragmentSelectorChoice} | null {
        const stillImgRepr = this.representations ? this.representations.getStillImage() : null;
        return stillImgRepr ? { repr: stillImgRepr } : null;
    }

    /**
     *  metadata map of resource
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public [S.meta](params?: { [p: string]: any }): any {
        if (this.metadata === undefined || !Array.isArray(this.metadata)) {
            return {};
        }
        const meta: any = {};
        this.metadata.forEach((item) => {
            meta[item.label] = item.value;
        });
        return meta;
    }
}


export class ResolvedPermissions extends JsonObject {
    public jsonClass = 'ResolvedPermissions';

    public read!: boolean;

    public updateContent!: boolean;

    public updateLinks!: boolean;

    public updatePermissions!: boolean;

    public createAnnos!: boolean;

    public createChildren!: boolean;

    public delete!: boolean;
}


export class Text extends Resource {
    public static from({ chars, language, script }: { chars: string, language?: string, script?: string}): Text {
        const doc = {
            jsonClass: 'Text',
            chars,
            language,
            script,
        };
        purgeUndefined(doc);
        return Resource.make<Text>(doc);
    }

    public jsonClass: string = 'Text';

    public chars!: string;

    public language?: string;

    public script?: string;
}

export class Annotation extends Resource {
    public jsonClass = 'Annotation';

    public type = 'oa:Annotation';

    public target!: string | string[];

    public body?: any;
}


ModelsRegistry.update({
    Resource,
    Text,
    Annotation,
    ResolvedPermissions,
    '*': Resource,
});
