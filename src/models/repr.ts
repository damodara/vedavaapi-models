import { purgeUndefined } from '../utils';
import { JsonObject, ModelsRegistry } from '.';

export class DataRepresentation extends JsonObject {
    public data!: string;

    /**
     * return oold fileId by stripping '_OOLD:' prefix in this.data
     */
    public getFileId(): string | null {
        if (!this.data) {
            return null;
        }
        return this.data.slice(6);
    }
}

export class StillImageRepresentation extends DataRepresentation {
    public static from({ fileId }: { fileId: string }): StillImageRepresentation {
        const doc = {
            jsonClass: 'StillImageRepresentation',
            data: `_OOLD:${fileId}`,
        };
        purgeUndefined(doc);
        return JsonObject.make<StillImageRepresentation>(doc);
    }

    public jsonClass: string = 'StillImageRepresentation';

    public implements?: string[];

    public supportsIIIF(): boolean {
        return this.implements !== undefined && Array.isArray(this.implements) && this.implements.includes('iiif_image');
    }
}

export class DataRepresentations extends JsonObject {
    public static from({ defaultRepr = 'stillImage', stillImageFileIds }: { stillImageFileIds?: string[], defaultRepr?: string }): DataRepresentations {
        const doc = {
            jsonClass: 'DataRepresentations',
            default: defaultRepr,
            stillImage: stillImageFileIds ? stillImageFileIds.map((fileId) => StillImageRepresentation.from({ fileId })) : undefined,
        };
        purgeUndefined(doc);
        return JsonObject.make<DataRepresentations>(doc);
    }

    public jsonClass: string = 'DataRepresentations';

    public default?: string;

    public stillImage?: StillImageRepresentation[];

    public interactiveResource?: DataRepresentation[];

    public getStillImage(): StillImageRepresentation | null {
        if (!this.stillImage || !Array.isArray(this.stillImage) || !this.stillImage.length || !this.stillImage[0]) {
            return null;
        }
        return this.stillImage[0];
    }
}


ModelsRegistry.update({
    DataRepresentation,
    StillImageRepresentation,
    DataRepresentations,
});
