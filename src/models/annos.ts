import * as S from '../symbols';
import { Annotation, Text, Resource } from './base';
import { purgeUndefined } from '../utils';
import { ModelsRegistry } from '.';

export class TextAnnotation extends Annotation {
    public static from({ target, text }: {target: string, text: string | Text}): TextAnnotation {
        const doc = {
            jsonClass: 'TextAnnotation',
            target,
            body: [typeof (text) === 'string' ? Text.from({ chars: text }) : text],
        };
        return Resource.make<TextAnnotation>(doc);
    }

    public jsonClass = 'TextAnnotation';

    public target!: string;

    public body?: Text[];

    public [S.title]() {
        if (!this.body || this.body.length === 0) {
            return null;
        }
        return this.body[0];
    }
}


export interface ISequenceAnnoBody {
    /** members of the sequence */
    members: Array<{
        /** index of a member */
        index: string | number;
        /** id of member resource */
        resource: string;
    }>;
}

export class SequenceAnnotation extends Annotation {
    public static from({ target, members, canonical }: {target: string; members: string[], canonical?: string}): SequenceAnnotation {
        const doc = {
            jsonClass: 'SequenceAnnotation',
            target,
            body: {
                members: members.map((resource, index) => ({ index, resource })),
            },
            canonical,
        };
        purgeUndefined(doc);
        return Resource.make<SequenceAnnotation>(doc);
    }

    public jsonClass = 'SequenceAnnotation';

    public target!: string;

    public body!: ISequenceAnnoBody;

    public canonical?: string;
}


ModelsRegistry.update({
    TextAnnotation,
    SequenceAnnotation,
});
