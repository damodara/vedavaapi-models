
export interface IResourceDoc {
    jsonClass: string;
    metadata?: Array<{ jsonClass: 'MetadataItem', label: string, value: any }>;
    source?: string | string[] | null;
    target?: string | string[] | null;
    body?: any;
    selector?: any;
    jsonClassLabel?: string;
    _id?: string;
    state?: 'system_inferred' | 'user_supplied' | 'user_edited' | 'user_confirmed' | 'user_rejected';
    [x: string]: any,
}

export const ModelsRegistry = {
    registry: new Map(),

    get(jsonClass: string): new () => JsonObject {
        return this.registry.get(jsonClass);
    },

    set(jsonClass: string, cls: new () => JsonObject): void {
        this.registry.set(jsonClass, cls);
    },

    update(map: { [jsonClass: string]: new () => JsonObject }): void {
        Object.keys(map).forEach((jsonClass) => {
            this.registry.set(jsonClass, map[jsonClass]);
        });
    },

    make<T extends JsonObject>(doc: IResourceDoc): T {
        ModelsRegistry.makeFrom(doc);
        return (doc as T);
    },

    makeFrom(val: any): any {
        if (['function'].includes(typeof (val))) {
            return val;
        }

        if (['number', 'string', 'boolean', 'undefined'].includes(typeof (val))) {
            return val;
        }

        if (val === null) {
            return val;
        }

        if (Array.isArray(val)) {
            val.forEach((item) => {
                ModelsRegistry.makeFrom(item);
            });
            return val;
        }

        // eslint-disable-next-line no-use-before-define
        if (val instanceof JsonObject) {
            return val;
        }

        if (Object.prototype.hasOwnProperty.call(val, 'jsonClass')) {
            // eslint-disable-next-line no-use-before-define
            const cls = ModelsRegistry.get(val.jsonClass) || ModelsRegistry.get('*') || JsonObject;
            Object.setPrototypeOf(val, cls.prototype);
        }
        Object.keys(val).forEach((k) => {
            ModelsRegistry.makeFrom(val[k]);
        });
        return val;
    },
};


export class JsonObject {
    public static make<T extends JsonObject>(doc: IResourceDoc): T {
        return ModelsRegistry.make<T>(doc);
    }

    public static makeFrom(val: any): any {
        return ModelsRegistry.makeFrom(val);
    }

    /**
 * make resource from jsonClass specific custom params
 * @param params any custom params
 */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public static from(params: any): JsonObject {
        return JsonObject.make<JsonObject>({
            jsonClass: 'JsonObject',
        });
    }

    public jsonClass = 'JsonObject';

    [x: string]: any,
}

ModelsRegistry.update({
    JsonObject,
});
