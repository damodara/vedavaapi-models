import { Resource } from './base';
import { ModelsRegistry } from '.';

export class Service extends Resource {
    public jsonClass = 'Service';

    public name!: string;

    public url!: string;

    public handledTypes!: string[];

    public apis?: string[]
}


ModelsRegistry.update({
    Service,
});
