import { Resource } from './base';
import { ModelsRegistry } from '.';


export class StillImage extends Resource {
    jsonClass = 'StillImage';

    namespace?: string;

    identifier?: string;

    url?: string;
}


ModelsRegistry.update({
    StillImage,
});
