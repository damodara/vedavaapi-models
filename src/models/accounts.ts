import * as S from '../symbols';
import { Resource, Text } from './base';
import { ModelsRegistry } from '.';


export class User extends Resource {
    public jsonClass = 'User';

    public name?: string;

    public email!: string;

    public password?: string;

    public [S.title](): Text | null {
        if (this.name === undefined) {
            return null;
        }
        return Text.from({ chars: this.name });
    }
}

export class Team extends Resource {
    public jsonClass = 'Team';

    public name!: string;

    public description?: string;

    public source?: string | null;

    public members?: string[];

    public [S.title](): Text {
        return Text.from({ chars: this.name });
    }

    public [S.dsc](): Text | null {
        return this.description !== undefined ? Text.from({ chars: this.description }) : null;
    }
}


ModelsRegistry.update({
    User,
    Team,
});
