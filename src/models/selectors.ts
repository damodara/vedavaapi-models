import { JsonObject, ModelsRegistry } from '.';


export class Selector extends JsonObject {
    public jsonClass!: string;

    public refinedBy?: Selector | SelectorChoice;
}

export interface IBBox {
    x: number;
    y: number;
    w: number;
    h: number;
}

export class FragmentSelector extends Selector {
    public static imageBBoxRegex = new RegExp('^xywh=([0-9]+),([0-9]+),([0-9]+),([0-9]+)$');

    public jsonClass = 'FragmentSelector';

    public type = 'oa:FragmentSelector';

    public value!: string;

    public conformsTo?: string;

    public get imageBBox(): IBBox | null {
        const match = this.value.match(FragmentSelector.imageBBoxRegex);
        if (!match) {
            return null;
        }
        const [, x, y, w, h] = match;
        return { x: Number(x), y: Number(y), w: Number(w), h: Number(h) };
    }
}

export class SvgSelector extends JsonObject {
    public jsonClass = 'SvgSelector';

    public value!: string;

    public type = 'oa:SvgSelector';
}


export class SelectorChoice extends Selector {
    public jsonClass!: string;

    public type = 'oa:Choice';

    public default?: Selector;

    public item?: Selector;
}


export class SvgFragmentSelectorChoice extends SelectorChoice {
    public jsonClass = 'SvgFragmentSelectorChoice';

    public default!: FragmentSelector;

    public item!: SvgSelector;

    public get getImageBBox(): IBBox | null {
        return this.default.imageBBox;
    }
}


ModelsRegistry.update({
    Selector,
    SelectorChoice,
    FragmentSelector,
    SvgSelector,
    SvgFragmentSelectorChoice,
});
